function Base(settings) {
  this.settings = merge({ tag: 'div', helpClass: 'wrap', uniqElement: false }, settings);
}

function merge(firstObject, secondObject) {
  if (!secondObject) {
    return firstObject;
  }

  for (var key in secondObject) {
    firstObject[key] = secondObject[key];
  }

  return firstObject;
}

Base.prototype = {
  constructor: Base,
  show: function() {
    var tag = document.createElement(this.settings.tag);
    tag.classList.add(this.settings.helpClass);
    return tag;
  }
};

function extend(Child, Parent) {
  Child.prototype = Object.create(Parent.prototype);
  Child.prototype.constructor = Child;
  Child.superclass = Parent.prototype;
}