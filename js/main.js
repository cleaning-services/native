window.onload = function() {
  document.getElementById('selector').addEventListener('click', toggleCompanyList);
  document.querySelector('.company-services').addEventListener('click', addToCard);
  document.querySelector('.companies-list').addEventListener('click', selectCompany);
  document.querySelector('.actions').addEventListener('click', openPopup);
  document.querySelector('.fa-home').addEventListener('click', openPopup);

  var modalEl = document.getElementById('modal-bg');
  modalEl.addEventListener('click', hidePopup);
  modalEl.querySelector('.close').addEventListener('click', hidePopup);
}

function Main() { }

displayCompanies();

var notify = new Notify({ tag: 'div', helpClass: 'banner' }),
    modal = new Modal({ tag: 'iframe', helpClass: 'modal-window', uniqElement: true });

function addToCard() {
  notify.show('info', 'You add this to card!');
}

function displayCompanies() {
  try {
    todoCompamies.forEach(function (item) {
      var ul = document.querySelector('.companies-list'),
          li = document.createElement("li");

      li.setAttribute('id', item.id);
      li.innerHTML = item.name;
      li.classList.add('item');
      ul.appendChild(li);
    });
    // TODO: Teporary
    selectCompany(1);
  } catch (error) {
    console.log(error);
  }
}

function toggleCompanyList() {
  document.querySelector('.companies-list').classList.toggle('opened');
}

function selectCompany(event) {
  var id = event.target ? event.target.id : event;
  if (!id) return;

  if (id == 5) { // TODO: For show error
    notify.show('error', 'Oops! Something went wrong!');
    return;
  }

  var selectedCompany = todoServices.find(function (item) {
    return item.id == id;
  });

  if (!selectedCompany) {
    notify.show('warning', 'This company locked');
    return;
  }

  toggleCompanyList();
  displayServices(selectedCompany);
}

function displayServices(selectedCompany) {
  try {
    var listWraper = document.querySelector('.company-services');
    listWraper.innerHTML = '';
    document.querySelector('.company-description').innerHTML = selectedCompany.description;
    document.querySelector('h1').innerHTML = selectedCompany.name;

    selectedCompany.services.forEach(function (item) {

      var div = document.createElement('div'),
          title = document.createElement('div'),
          price = document.createElement('strong'),
          image = document.createElement('img');

      div.classList.add('product-content');
      title.innerHTML = item.name;
      price.innerHTML = item.price + ' USD';
      image.setAttribute('src', item.img);

      div.appendChild(image);
      div.appendChild(title);
      div.appendChild(price);
      listWraper.appendChild(div);
    });
  } catch (error) {
    console.log(error);
  }
}

function openPopup(event) {
  modal.show(event.target.dataset.component);
}

function hidePopup(event) {
  if (event.target.id === 'modal-bg' || event.target.id === 'close') {
    modal.hide();
  }
}

(function(){
  notify.show('success', 'You login successfully!');
})()