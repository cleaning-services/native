function Modal(settings) {
  Base.call(this, settings);
  Modal.superclass.constructor.call(this, settings);
}

extend(Modal, Base);

Modal.prototype.show = function(component) {
  var iframe = Base.prototype.show.apply(this, arguments);

  var modal = document.getElementById('modal-bg');
  var modalContent = modal.querySelector('.content');
  modalContent.className = 'content ' + component;

  iframe.setAttribute('src', './templates/' + component + '.html');

  modalContent.appendChild(iframe);
  modal.style.display = 'flex';
  return null;
}

Modal.prototype.hide = function() {
  var modal = document.getElementById('modal-bg');
  var modalContent = modal.querySelector('.content');
  var iframe = document.querySelector('iframe');
  if (iframe) {
    modalContent.removeChild(document.querySelector('iframe'));
  }
  modal.querySelector('.content').classList.add('closed');
  setTimeout(function() {
    modal.style.display = 'none';
  }, 200);
}