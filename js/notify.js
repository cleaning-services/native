function Notify(settings) {
  // Base.call(this, settings);
  Notify.superclass.constructor.call(this, settings);
}

extend(Notify, Base);

Notify.prototype.show = function (type, text) {

  Base.prototype.show.apply(this, arguments);
  
  var banner = Base.prototype.show.apply(this, arguments),
      bannerRow = document.querySelector('.notifications'),
      bannerContent = document.createElement('div'),
      icon = document.createElement('i'),
      textWrapper = document.createElement('div'),
      title = document.createElement('p'),
      textEl = document.createElement('p');

  bannerContent.classList.add('content');


  icon.classList.add('fa', 'fa-times-circle');

  title.classList.add('title');
  title.innerHTML = type;
  
  textEl.classList.add('text');
  textEl.innerHTML = text;

  textWrapper.appendChild(title);
  textWrapper.appendChild(textEl);

  bannerContent.appendChild(textWrapper);
  bannerContent.appendChild(icon);

  banner.appendChild(bannerContent);
  banner.classList.add('banner', type);
  bannerRow.insertBefore(banner, bannerRow.children[0]);

  icon.addEventListener('click', function () {
    clearTimeout(timeOut);
    bannerRow.removeChild(banner);
  });

  var timeOut = setTimeout(function () {
    bannerRow.removeChild(banner);
  }, 5000);

  return null;
}
