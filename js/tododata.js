var todoCompamies = [
  { id: 1, name: 'Clean kitchen' },
  { id: 2, name: 'Cache cleaner' },
  { id: 3, name: 'Coockie clicker' },
  { id: 4, name: 'Fixtures uploaders' },
  { id: 5, name: 'Mongoose with me' },
  { id: 6, name: 'Locked company' },
  { id: 7, name: 'Locked company' },
  { id: 8, name: 'Locked company' },
  { id: 9, name: 'Locked company' },
];

var todoServices = [
  {
    id: 1,
    name: 'Clean kitchen',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. T',
    services: [
      { id: 1, name: 'Чистка кухни', price: '110', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-007_1.jpg' },
      { id: 2, name: 'Чистка мебели', price: '120', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-008_1.jpg' },
      { id: 3, name: 'Чистка санузла', price: '130', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-001_1.jpg' },
      { id: 4, name: 'Чистка сада', price: '140', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg' },
      { id: 5, name: 'Чистка крыши', price: '150', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-003_1.jpg' }
    ]
  },
  {
    id: 2,
    name: 'Cache cleaner',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    services: [
      { id: 1, name: 'Чистка кухни', price: '210',  img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-007_1.jpg' },
      { id: 2, name: 'Чистка мебели', price: '220', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-008_1.jpg' },
      { id: 4, name: 'Чистка сада', price: '240', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-001_1.jpg' },
      { id: 5, name: 'Чистка крыши', price: '250',  img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg' }
    ]
  },
  {
    id: 3,
    name: 'Coockie clicker',
    description: 'dle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined',
    services: [
      { id: 1, name: 'Чистка кухни', price: '310',  img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-007_1.jpg' },
    ]
  },
  {
    id: 4,
    name: 'Fixtures uploaders',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    services: [
      { id: 1, name: 'Чистка кухни', price: '410',  img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-007_1.jpg' },
      { id: 2, name: 'Чистка мебели', price: '420', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg'  },
      { id: 3, name: 'Чистка санузла', price: '430', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg'  },
      { id: 4, name: 'Чистка сада', price: '440', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg' },
      { id: 5, name: 'Чистка крыши', price: '450', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-008_1.jpg' },
      { id: 1, name: 'Чистка кухни 2', price: '410', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-008_1.jpg' },
      { id: 2, name: 'Чистка мебели 2', price: '420', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-008_1.jpg' },
      { id: 3, name: 'Чистка санузла 2', price: '430', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg' },
      { id: 4, name: 'Чистка сада 2', price: '440', img: 'https://i1.proskater.ru/images/bimages/1211/15062018-7414a-007_1.jpg' },
      { id: 5, name: 'Чистка крыши 2', price: '450', img: 'https://i1.proskater.ru/images/bimages/1221/04092018-7686a-002_1.jpg' }
    ]
  },
  {
    id: 5,
    name: 'Mongoose with me',
    services: [
    ]
  },
];